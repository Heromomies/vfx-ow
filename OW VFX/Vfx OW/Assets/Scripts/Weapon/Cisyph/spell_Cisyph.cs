﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spell_Cisyph : MonoBehaviour
{
    [Header("Base datas")]
    public float fireRate = 1.5f;
    public GameObject knife, player;
    public Transform firepoint;
    public float knife_speed;
    public float range = 100;
    public LayerMask layers;
    public static int action;
    private Vector3 pos;
    private Quaternion rot;
    private Vector3 shootpoint;

    [Header("Spell 1 && Spell 2")] 
    public float fireRate_spellA = 5f;

    public  bool spellOne;
    public float spell1_speed;
    public GameObject spell_1,spell1two;
    public Transform spell_1_transform, spell1two_transform;
    public ParticleSystem spell2;
    public GameObject ennemy;
    private static spell_Cisyph _instance;

    public static spell_Cisyph _spellCisyph => _instance;
    // Start is called before the first frame update
    void Start()
    {
        _instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        fireRate -= Time.deltaTime;
        //Permet que le fireRate ne descende pas endessous de 0
        fireRate = Mathf.Clamp(fireRate, 0, Mathf.Infinity);
        fireRate_spellA -= Time.deltaTime;
        fireRate_spellA = Mathf.Clamp(fireRate_spellA, 0, Mathf.Infinity);
        //Appele la method shoot
        if (Input.GetMouseButtonDown(0) && fireRate <= 0f)
        {
            action = 0;
            Shoot();
        }


        if (Input.GetKeyDown(KeyCode.A) && fireRate_spellA <= 0f)
        {
            
            action = 1;
            TripwireBlade();
            
        }

        if (Input.GetKeyDown(KeyCode.E) && fireRate_spellA <= 0f)
        {
            BladeRecall();
        }




    }
    private void FixedUpdate()
    {
        
        pos = firepoint.position;
        rot = firepoint.rotation;
        shootpoint = firepoint.forward;

    }
    void Shoot()
    {
        RaycastHit raycastHit;
       
        
     
        Debug.DrawRay(pos, Camera.main.transform.forward * 100, Color.magenta, 3f);
        GameObject knife = Instantiate(this.knife, pos, rot);
       Physics.IgnoreCollision(knife.GetComponentInChildren<BoxCollider>(),
           player.GetComponent<CharacterController>());
        knife.GetComponent<Rigidbody>().AddForce(shootpoint * knife_speed, ForceMode.Impulse);
        
        fireRate = 0.4f;
            
        if (Physics.Raycast(firepoint.position, Camera.main.transform.forward, out raycastHit,range,layers  ))
        {
            Debug.Log(raycastHit.collider.name);
         
        }
    }

   public void TripwireBlade()
   {
     
           GameObject spell1 = Instantiate(spell_1, spell_1_transform.position, rot);
           GameObject spell = Instantiate(spell1two, spell1two_transform.position, rot);
           Physics.IgnoreCollision(spell1.GetComponentInChildren<BoxCollider>(),
               player.GetComponent<CharacterController>());
           Physics.IgnoreCollision(spell.GetComponentInChildren<BoxCollider>(),
               player.GetComponent<CharacterController>());
           spell1.GetComponent<Rigidbody>().AddForce(shootpoint * spell1_speed, ForceMode.Impulse);
           spell.GetComponent<Rigidbody>().AddForce(shootpoint * spell1_speed, ForceMode.Impulse);
           fireRate_spellA = 5f;
    }

   public void BladeRecall()
   {
     GameObject[] spell2 =  GameObject.FindGameObjectsWithTag("knife_spell2");
     ParticleSystem spel_2 = Instantiate(this.spell2, ennemy.transform.position, Quaternion.identity);
     
     
     foreach (GameObject knife in spell2)
     {
         knife.transform.SetParent(null);
         Rigidbody rb  = knife.AddComponent<Rigidbody>();
         rb.AddForce(Vector3.back * 15, ForceMode.Impulse);
            
     }
     
   }
    

   
   
}

