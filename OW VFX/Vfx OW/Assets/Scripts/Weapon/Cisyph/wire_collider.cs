﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wire_collider : MonoBehaviour
{
   
    private Vector3 pos;
    public List<ParticleCollisionEvent> _collisionEvents;
    private GameObject enmemy_pos;
    public ParticleSystem stunt_effect;
    // Start is called before the first frame update
    void Start()
    {
        
        _collisionEvents = new List<ParticleCollisionEvent>();
        foreach (ParticleCollisionEvent collision in _collisionEvents)
        {
            pos = collision.intersection;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnParticleCollision(GameObject other)
    {

        ParticleSystem effect  = Instantiate(stunt_effect, other.transform.position , Quaternion.identity);
        Destroy(effect, 2f);
        
    }
}
