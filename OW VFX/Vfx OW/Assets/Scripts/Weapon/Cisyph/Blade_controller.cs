﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade_controller : MonoBehaviour
{
    public Collider sharpEnd;

    private Rigidbody _rigidbody;

    public GameObject particleSystem;

    public GameObject wire;

    public Transform transform_a;
    public Transform transform_b;

    

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();


    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (ContactPoint contactPoint in collision.contacts)
        {
            // Si le collider qui est hit est egale au col du knife alors 
            if (contactPoint.thisCollider == sharpEnd &&
                collision.contacts[0].otherCollider.gameObject.CompareTag("Enemy"))
            {

                Debug.Log(collision.collider.name);
                // is it going fast enough?
                if (_rigidbody.velocity.magnitude >= 1)
                {

                    // Permet de set le parent du couteau est donc qu'il reste planter sur l'enemy
                    transform.SetParent(contactPoint.otherCollider.transform.root, true);
                    //Désactive le col pour éviter les bugs
                    sharpEnd.enabled = false;
                    // Détruit le component rigidbody pour empécher le couteau de partir
                    Destroy(GetComponent<Rigidbody>());
                    if (spell_Cisyph.action == 0)
                    {
                        GameObject vfx_blood = Instantiate(particleSystem, collision.contacts[0].point,
                            Quaternion.identity);
                        Destroy(vfx_blood, 2f);
                    }


                    Destroy(gameObject, 6f);
                }
            }
            else if (contactPoint.thisCollider == sharpEnd &&
                     collision.contacts[0].otherCollider.gameObject.CompareTag("Wall") && spell_Cisyph.action == 1)
            {
                if (spell_Cisyph.action == 1)
                {
                  
                  if (wire.GetComponentInChildren<ParticleSystem>())
                  {
                      wire.GetComponentInChildren<ParticleSystem>().Stop();
                  }
                }

                if (_rigidbody.velocity.magnitude >= 1)
                {

                    // Permet de set le parent du couteau est donc qu'il reste planter sur l'enemy
                    transform.SetParent(contactPoint.otherCollider.transform.root, true);
                    //Désactive le col pour éviter les bugs
                    sharpEnd.enabled = false;
                    // Détruit le component rigidbody pour empécher le couteau de partir

                    Destroy(GetComponent<Rigidbody>());
                    Destroy(gameObject, 6f);
                }




            }
            else
            {
                Destroy(gameObject);
            }
        }

    }
}